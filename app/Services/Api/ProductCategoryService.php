<?php
namespace App\Services\Api;

use App\Services\Service;
use Illuminate\Pagination\LengthAwarePaginator as Pagination;

/**
 * ProductCategoryService
 */
class ProductCategoryService extends Service
{
    protected $model = 'App\Models\ProductCategory';

    public function getList()
    {
        $data = $this->model::with('parent', 'products', 'children')
            ->select()
            ->orderBy('updated_at', 'DESC')
            ->paginate($this->limit);
        $dataTransform = $data->map(function ($item) {
            $itemModel                 = $item->toArray();
            $itemModel['parent_name']  = $item->parent->name ?? 'No Parent';
            $itemModel['check_remove'] = $this->checkRemoveProduct($item);
            return $itemModel;
        });
        $data = new Pagination($dataTransform, $data->total(), $data->perPage());
        return $data;
    }

    public function checkRemoveProduct($item)
    {
        if ($item->parent_id != 0) {
            return true;
        }
        if ($item->products()->count() != 0 || $item->children()->count() != 0) {
            return false;
        }
        return true;
    }

    public function getParentList($id)
    {
        if ($id != 0) {
            $model = $this->getById($id);
            if ($model->children()->count() > 0) {
                return [];
            }
        }
        // just dad and children category
        return $this->model::select()->whereNotIn('id', [$id])->where('parent_id', 0)->orderBy('updated_at', 'DESC')->get();
    }

    public function getListParentToChild()
    {
        $data = $this->model::with('children')
            ->select()
            ->whereNotIn('parent_id', [0])
            ->orderBy('created_at', 'DESC')
            ->get()->transform(function ($item) {
            return [
                'name' => $item->name,
                'id'   => $item->id,
            ];
        });
        return $data;
    }
}
