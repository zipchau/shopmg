<?php
namespace App\Services\Api;

use App\Services\Service;
use Illuminate\Pagination\LengthAwarePaginator as Pagination;

/**
 * UserService
 */
class ShopService extends Service
{
    protected $model = 'App\Models\Shop';

    public function getTypeList()
    {
        return config('form.shop.type');
    }

    public function getStatusList()
    {
        return config('form.shop.status');
    }

    public function getList()
    {
        $data = $this->model::with('products')
            ->select()
            ->orderBy('updated_at', 'DESC')
            ->paginate($this->limit);
        $dataTransform = $data->map(function ($item) {
            $itemModel                  = $item->toArray();
            $itemModel['type']          = $this->getTypeList()[$item->type];
            $itemModel['status']        = $this->getStatusList()[$item->status];
            $itemModel['certification'] = $item->certification == 1 ? "Yes" : "No";
            $itemModel['check_remove']  = !$item->products()->exists();
            return $itemModel;
        });
        $data = new Pagination($dataTransform, $data->total(), $data->perPage());
        return $data;
    }

    public function storeOrUpdate($request, $id = null)
    {
        if ($id) {
            $model = $this->getById($id);
            $model->update($request);
            $this->handleUpdateImage($model, $request);
        } else {
            $model = $this->model::create($request);
            foreach ($request['images'] as $item) {
                $model->images()->create(['src' => $item]);
            }
        }
        return $model;
    }

    public function getByIdTransform($id)
    {
        $model = $this->model::with('images')->findOrFail($id);
        return [
            'type'          => $model->type,
            'name'          => $model->name,
            'slug'          => $model->slug,
            'sub_info'      => $model->sub_info,
            'information'   => $model->information,
            'status'        => $model->status,
            'certification' => $model->certification,
            'images'        => $model->images()->get()->transform(function ($item) {
                return [
                    'src'  => $item->src,
                    'size' => file_exists($item->src) ? filesize($item->src) : 0,
                ];
            }),
        ];
    }

    public function getListForFormProduct()
    {
        $data = $this->model::select('name', 'id')
            ->orderBy('created_at', 'DESC')
            ->get()->transform(function ($item) {
            return [
                'name' => $item->name,
                'id'   => $item->id,
            ];
        });
        return $data;
    }
}
