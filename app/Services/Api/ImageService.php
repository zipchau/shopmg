<?php
namespace App\Services\Api;

use File;

/**
 * ImageService
 */
class ImageService
{
    protected function get_image_type($target_file)
    {
        $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
        return $imageFileType;
    }

    protected function remove_extension_from_image($image)
    {
        $extension = $this->get_image_type($image); //get extension
        $only_name = basename($image, '.' . $extension); // remove extension
        return $only_name;
    }
    public function convert_image($convert_type, $target_dir, $image_name, $image_quality = 60)
    {
        $target_dir    = getenv('DOCUMENT_ROOT') . "$target_dir/";
        $image         = $target_dir . $image_name;
        $img_name      = $this->remove_extension_from_image($image);
        $img_name_type = pathinfo($image, PATHINFO_EXTENSION);

        //to png
        if ($convert_type == 'png') {
            $binary = imagecreatefromstring(file_get_contents($image));
            //third parameter for ImagePng is limited to 0 to 9
            //0 is uncompressed, 9 is compressed
            //so convert 100 to 2 digit number by dividing it by 10 and minus with 10
            $image_quality = floor(10 - ($image_quality / 10));
            ImagePNG($binary, $target_dir . $img_name . '.' . $convert_type, $image_quality);
            return $img_name . '.' . $convert_type;
        }

        //to jpg
        if ($convert_type == 'jpg') {
            $binary = imagecreatefromstring(file_get_contents($image));
            imageJpeg($binary, $target_dir . $img_name . '.' . $convert_type, $image_quality);
            return $img_name . '.' . $convert_type;
        }
        // origin
        if ($convert_type == 'ori') {
            return $img_name . '.' . $img_name_type;
        }
        if ($convert_type == 'gif') {
            $binary = imagecreatefromstring(file_get_contents($image));
            imageGif($binary, $target_dir . $img_name . '.' . $convert_type, $image_quality);
            return $img_name . '.' . $convert_type;
        }
        return false;
    }

    public function handleUpload($dir = 'uploads', $request)
    {
        $file      = request()->file;
        $date      = date("Ymd");
        $file_name = rand() . '_' . $date . '.' . $file->getClientOriginalExtension();
        if ($file->move($dir, $file_name)) {
            $imageout = $this->convert_image('jpg', '/' . $dir, $file_name);
            File::move($dir . '/' . $imageout, $dir . '/c' . $imageout);
            $newname = $dir . '/c' . $imageout;
            File::delete($dir . '/' . $file_name);
            return json_encode([
                'status'      => true,
                'filename'    => $newname,
                'type'        => 'jpg',
            ]);
        }
        return json_encode(['status' => false]);
    }

    public static function handleRemoveByName($name)
    {
        if (File::delete($name)) {
            return json_encode(['status' => true, 'filename' => $name, 'method' => 'delete']);
        }
        return json_encode(['status' => false]);
    }
}
