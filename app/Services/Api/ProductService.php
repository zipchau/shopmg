<?php
namespace App\Services\Api;

use App\Services\Service;
use Illuminate\Pagination\LengthAwarePaginator as Pagination;

/**
 * ProductService
 */
class ProductService extends Service
{
    protected $model = 'App\Models\Product';

    public function getList()
    {
        $data = $this->model::with('category')
            ->select()
            ->orderBy('updated_at', 'DESC')
            ->paginate($this->limit);
        $dataTransform = $data->map(function ($item) {
            $itemModel                  = $item->toArray();
            $itemModel['category_name'] = $item->category->name ?? 'No Category';
            return $itemModel;
        });
        $data = new Pagination($dataTransform, $data->total(), $data->perPage());
        return $data;
    }

    public function storeOrUpdate($request, $id = null)
    {
        if ($id) {
            $model = $this->getById($id);
            $model->update($request);
            $this->handleUpdateImage($model, $request);
        } else {
            $model = $this->model::create($request);
            foreach ($request['images'] as $item) {
                $model->images()->create(['src' => $item]);
            }
        }
        return $model;
    }
    public function getById($id)
    {
        return $model = $this->model::with('images')->findOrFail($id);
    }

    public function getByIdTransform($id)
    {
        $model = $this->getById($id);
        return [
            'name'        => $model->name,
            'category_id' => $model->category_id,
            'shop_id'     => $model->shop_id,
            'slug'        => $model->slug,
            'price_sale'  => $model->price_sale,
            'price'       => $model->price,
            'content'     => $model->content,
            'active'      => $model->active,
            'images'      => $model->images()->get()->transform(function ($item) {
                return [
                    'src'  => $item->src,
                    'size' => file_exists($item->src) ? filesize($item->src) : 0,
                ];
            }),
        ];
    }

}
