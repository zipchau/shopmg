<?php
namespace App\Services\Api;

use App\Services\Service;

/**
 * UserService
 */
class UserService extends Service
{
    protected $model = 'App\Models\User';
}
