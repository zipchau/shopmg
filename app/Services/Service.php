<?php
namespace App\Services;

use App\Models\Image;
use File;

class Service
{
    protected $limit = 10;
    protected $model;

    public function getList()
    {
        return $this->model::paginate($this->limit);
    }

    public function storeOrUpdate($request, $id = null)
    {
        if ($id) {
            $data = $this->getById($id);
            $data->update($request);
        } else {
            $data = $this->model::create($request);
        }
        return $data;
    }

    public function destroyById($id)
    {
        return (bool) $this->model::whereId($id)->delete();
    }

    public function getById($id)
    {
        return $this->model::findOrFail($id);
    }

    public function handleUpdateImage($model, $request)
    {
        $imagesModel = $model->images()->pluck('src')->toArray();
        foreach ($request['images'] as $value) {
            if (!in_array($value, $imagesModel)) {
                $model->images()->create(['src' => $value]);
            } else {
                $pos = array_search($value, $imagesModel);
                unset($imagesModel[$pos]);
            }
        }
        if (sizeof($imagesModel) > 0) {
            $listDelete = Image::whereIn('src', $imagesModel)->get();
            foreach ($listDelete as $value) {
                $value->delete();
                File::delete($value->src);
            }

        }
    }
}
