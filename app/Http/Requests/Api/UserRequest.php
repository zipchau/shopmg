<?php

namespace App\Http\Requests\Api;

use Hash;
use Illuminate\Foundation\Http\FormRequest;
use Validator;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        Validator::extend('tel', function ($attribute, $value, $parameters) {
            return preg_match("/[0-9]{10,11}/", $value);
        });

        Validator::extend('check_old_password', function ($attribute, $value, $parameters) {
            $user = User::find($parameters[0]);
            if (!$user) {
                return false;
            }
            if (Hash::check($value, $user->password)) {
                return true;
            } else {
                return false;
            }
        });

        $rules = [
            'email'                 => 'required|min:6|email|unique:users,email,' . $this->user,
            'password'              => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
            'name'                  => 'required|min:6',
            // 'phone'                 => 'tel',
        ];
        if ($this->method() == 'PUT') {
            if (!isset($this->password) || strlen(trim($this->password)) == 0) {
                unset($rules['password']);
                unset($rules['password_confirmation']);
            }
        }
        return $rules;
    }

    public function attributes()
    {
        return [
            'email'                 => 'Email',
            'password'              => 'Password',
            'password_confirmation' => 'Password Confirmation',
            'name'                  => 'Name',
            'phone'                 => 'Phone',
        ];
    }

    public function messages()
    {
        return [
            'phone.tel' => 'the :attributes does not match the format 10 number',
        ];
    }
}
