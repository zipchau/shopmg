<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class ProductCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // $this->merge(
        //     [
        //         'slug' => str_slug($this->name),
        //     ]
        // );
        return [
            "name"   => "required|unique:product_categories,name",
            "parent" => "integer",
        ];
    }

    public function attributes()
    {
        return [
            "name"   => "Name",
            "parent" => "Parent",
        ];
    }
}
