<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\ImageRequest;
use App\Services\Api\ImageService;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    protected $imageService;

    public function __construct(ImageService $imageService)
    {
        $this->imageService = $imageService;
    }

    public function storeApi(ImageRequest $request)
    {
        return $this->imageService->handleUpload('uploads', $request);
    }

    public function removeByNameApi(Request $request)
    {
        $name = $request->nameImage;
        return $this->imageService->handleRemoveByName($name);
    }
}
