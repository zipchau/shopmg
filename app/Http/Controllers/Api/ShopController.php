<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\ShopRequest;
use App\Services\Api\ShopService;
use Helper;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    protected $shopService;

    public function __construct(ShopService $shopService)
    {
        $this->shopService = $shopService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->shopService->getList()->toArray();
        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\ShopRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ShopRequest $request)
    {
        $data = $this->shopService->storeOrUpdate($request->all());
        return Helper::returnApiSuccess($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->shopService->getByIdTransform($id);
        return Helper::returnApiSuccess($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\ShopRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ShopRequest $request, $id)
    {
        $data = $this->shopService->storeOrUpdate($request->all(), $id);
        return Helper::returnApiSuccess($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = $this->shopService->destroyById($id);
        return Helper::returnApiSuccess($data);
    }

    public function getConfig()
    {
        return [
            'type'   => $this->shopService->getTypeList(),
            'status' => $this->shopService->getStatusList(),
        ];
    }

    public function getListForFormProduct()
    {
        $data = $this->shopService->getListForFormProduct();
        return Helper::returnApiSuccess($data);

    }
}
