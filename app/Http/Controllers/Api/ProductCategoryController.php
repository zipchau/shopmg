<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\ProductCategoryRequest;
use App\Services\Api\ProductCategoryService;
use Helper;
use Illuminate\Http\Request;

class ProductCategoryController extends Controller
{
    protected $productCategoryService;

    public function __construct(ProductCategoryService $productCategoryService)
    {
        $this->productCategoryService = $productCategoryService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->productCategoryService->getList()->toArray();
        return $data;
        // return Helper::returnApiSuccess($data);
    }

    public function getParentList($id)
    {
        $data = $this->productCategoryService->getParentList($id);
        return Helper::returnApiSuccess($data);
    }

    public function getListParentToChild()
    {
        $data = $this->productCategoryService->getListParentToChild();
        return Helper::returnApiSuccess($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\ProductCategoryRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductCategoryRequest $request)
    {
        $data = $this->productCategoryService->storeOrUpdate($request->all());
        return Helper::returnApiSuccess($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->productCategoryService->getById($id)->toArray();
        return Helper::returnApiSuccess($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\ProductCategoryRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductCategoryRequest $request, $id)
    {
        $data = $this->productCategoryService->storeOrUpdate($request->all(), $id);
        return Helper::returnApiSuccess($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = $this->productCategoryService->destroyById($id);
        return Helper::returnApiSuccess($data);
    }
}
