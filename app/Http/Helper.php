<?php
namespace App\Http;

/**
 * Helper
 */
class Helper
{
    public static function returnApiSuccess($data)
    {
        $data = [
            'status' => true,
            'data'   => $data,
        ];
        return response()->json($data);
    }

    public static function returnApiError($message = false)
    {
        $data = [
            'status'  => false,
            'message' => $message,
        ];
        return response()->json($data);
    }
}
