<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = ['src', 'alt', 'description'];

    public function imageable()
    {
        return $this->morphTo();
    }
}
