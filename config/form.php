<?php
return [

    // config shop
    'shop' => [
        'type' => [
            '1' => 'in house',
            '2' => 'personal',
            '3' => 'enterprise',
        ],
        'status' =>[
        	'1' => 'close',
        	'2' => 'pedding',
        	'3' => 'active',
        ]
    ],

    //
];
