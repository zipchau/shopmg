<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

// Route::group(['middleware' => 'auth:api'], function () {
//     Route::post('logout', 'Auth\LoginController@logout');

//     Route::get('/user', function (Request $request) {
//         return $request->user();
//     });

//     Route::patch('settings/profile', 'Settings\ProfileController@update');
//     Route::patch('settings/password', 'Settings\PasswordController@update');
// });

Route::group(['middleware' => 'guest:api', 'namespace' => 'Api'], function () {
    //image
    Route::post('image/storeapi', 'ImageController@storeApi');
    Route::post('image/removebynameapi', 'ImageController@removeByNameApi');

    //product category
    Route::get('product-category/parent/{id}', 'ProductCategoryController@getParentList');
    Route::get('product-category/parenttochild', 'ProductCategoryController@getListParentToChild');

    //shop
    Route::get('shop/getconfig', 'ShopController@getConfig');
    Route::get('shop/getlistforproduct', 'ShopController@getListForFormProduct');

    //resource
    Route::resource('product-category', 'ProductCategoryController')->only(['index', 'show', 'store', 'update', 'destroy']);
    Route::resource('shop', 'ShopController')->only(['index', 'show', 'store', 'update', 'destroy']);
    Route::resource('user', 'UserController')->only(['index', 'show', 'store', 'update', 'destroy']);
    Route::resource('product', 'ProductController')->only(['index', 'show', 'store', 'update', 'destroy']);
});
