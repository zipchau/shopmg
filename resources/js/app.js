import Vue from 'vue'
import store from '~/store'
import router from '~/router'
import i18n from '~/plugins/i18n'
import App from '~/components/App'
import BootstrapVue from 'bootstrap-vue'
import vSelect from 'vue-select'
import '~/plugins'
import '~/components'

import VueQuillEditor from 'vue-quill-editor'



Vue.use(VueQuillEditor)

Vue.use(BootstrapVue)
Vue.component('v-select', vSelect)
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  i18n,
  store,
  router,
  ...App
})
