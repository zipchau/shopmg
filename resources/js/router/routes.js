const Index = () => import('~/pages/index').then(m => m.default || m)
const Home = () => import('~/pages/Home').then(m => m.default || m)

const ListUser = () => import('~/pages/user/list').then(m => m.default || m)
const FormUser = () => import('~/pages/user/form').then(m => m.default || m)

const IndexProductCategory = () => import('~/pages/product_category/index').then(m => m.default || m)
const ListProductCategory = () => import('~/pages/product_category/list').then(m => m.default || m)
const FormProductCategory = () => import('~/pages/product_category/form').then(m => m.default || m)

const IndexProduct = () => import('~/pages/product/index').then(m => m.default || m)
const ListProduct = () => import('~/pages/product/list').then(m => m.default || m)
const FormProduct = () => import('~/pages/product/form').then(m => m.default || m)

const IndexShop = () => import('~/pages/shop/index').then(m => m.default || m)
const ListShop = () => import('~/pages/shop/list').then(m => m.default || m)
const FormShop = () => import('~/pages/shop/form').then(m => m.default || m)
export default [
  {
    path: '/', redirect: '/dashboard', name: 'Index', component: Index,
    children: [
    	{ path: 'dashboard', name: 'Dashboard', component: Home },
    	//category-product
      {path: '/product-category', redirect: '/product-category/index', name: 'Product Category', component: Index,
      children: [
  		  { path: 'index', component: ListProductCategory},
  		  { path: 'create', name: 'Create Product Category', component: FormProductCategory },
  		  { path: 'update/:id', name: 'Update Product Category', component: FormProductCategory },
  	  ]},
      //product
      {path: '/product', redirect: '/product/index', name: 'Product', component: Index,
      children: [
        { path: 'index', component: ListProduct},
        { path: 'create', name: 'Create Product', component: FormProduct },
        { path: 'update/:id', name: 'Update Product', component: FormProduct },
      ]},
      //shop
      {path: '/shop', redirect: '/shop/index', name: 'Shop', component: Index,
      children: [
        { path: 'index', component: ListShop},
        { path: 'create', name: 'Create Shop', component: FormShop },
        { path: 'update/:id', name: 'Update Shop', component: FormShop },
      ]},
      //user
  		{ path: 'user', name: 'ListUser', component: ListUser},
  		{ path: 'user/create', name: 'CreateUser', component: FormUser },
  		{ path: 'user/update/:id', name: 'UpdateUser', component: FormUser },
    ]
  },
]
