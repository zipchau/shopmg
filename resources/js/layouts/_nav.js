export default {
  items: [
    {
      name: 'Dashboard',
      url: '/',
      icon: 'icon-speedometer',
    },
    {
      title: true,
      name: 'Shop',
      class: '',
      wrapper: {
        element: '',
      }
    },
    {
      name: 'Shop',
      url: '/shop',
      icon: 'icon-handbag',
      children: [
        {
          name: 'List',
          url: '/shop/index'
        },
        {
          name: 'Add New',
          url: '/shop/create'
        }
      ]
    },
    {
      title: true,
      name: 'Product',
      class: '',
      wrapper: {
        element: '',
      }
    },
    {
      name: 'Category',
      url: '/product-category',
      icon: 'icon-list',
      children: [
        {
          name: 'List',
          url: '/product-category/index'
        },
        {
          name: 'Add New',
          url: '/product-category/create'
        }
      ]
    },
    {
      name: 'Product',
      url: '/product',
      icon: 'icon-present',
      children: [
        {
          name: 'List',
          url: '/product/index'
        },
        {
          name: 'Add new',
          url: '/product/create'
        }
      ]
    },
    {
      title: true,
      name: 'User',
      class: '',
      wrapper: {
        element: '',
        attributes: {}
      }
    },
    {
      name: 'User',
      url: '/user',
      icon: 'icon-user',
      children: [
        {
          name: 'List',
          url: '/user',
        },
        {
          name: 'Add new',
          url: '/user/create',
        }
      ]
    }
  ]
}
