import axios from 'axios'
import * as types from '../mutation-types'

// state
export const state = {
  listProduct: [],
  productById: []
}

// getters
export const getters = {
  listProduct: state => state.listProduct,
  productById: state => state.productById.data
}

// mutations
export const mutations = {
  [types.FETCH_PRODUCT] (state, { data }) {
    state.listProduct = data
  },
  [types.UPDATE_PRODUCT] (state, { data }) {
    state.productById = data
  }
}

// actions
export const actions = {
  async fetchProduct ({ commit },page) {
      const { data } = await axios.get('/api/product?page='+page.page)
      commit(types.FETCH_PRODUCT, { data: data })
  },

  async fetchProductById ({ commit },id) {
      const { data } = await axios.get('/api/product/'+id.id)
      commit(types.UPDATE_PRODUCT, { data: data })
  }
}
