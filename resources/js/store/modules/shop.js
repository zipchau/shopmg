import axios from 'axios'
import * as types from '../mutation-types'

// state
export const state = {
  listShop: [],
  shopById: [],
  configShop: [],
  listForProduct:[]
}

// getters
export const getters = {
  listShop: state => state.listShop,
  shopById: state => state.shopById.data,
  configShop: state => state.configShop,
  listForProduct: state => state.listForProduct.data
}

// mutations
export const mutations = {
  [types.FETCH_SHOP] (state, { data }) {
    state.listShop = data
  },
  [types.FETCH_LISTFORPRODUCT] (state, { data }) {
    state.listForProduct = data
  },
  [types.FETCH_CONFIG_SHOP] (state, { data }) {
    state.configShop = data
  },
  [types.UPDATE_SHOP] (state, { data }) {
    state.shopById = data
  }
}

// actions
export const actions = {
  async fetchConfigShop ({ commit },page) {
      const { data } = await axios.get('/api/shop/getconfig')
      commit(types.FETCH_CONFIG_SHOP, { data: data })
  },
  async fetchListForProduct ({ commit },page) {
      const { data } = await axios.get('/api/shop/getlistforproduct')
      commit(types.FETCH_LISTFORPRODUCT, { data: data })
  },
  async fetchShop ({ commit },page) {
      const { data } = await axios.get('/api/shop?page='+page.page)
      commit(types.FETCH_SHOP, { data: data })
  },

  async fetchShopById ({ commit },id) {
      const { data } = await axios.get('/api/shop/'+id.id)
      commit(types.UPDATE_SHOP, { data: data })
  }
}
