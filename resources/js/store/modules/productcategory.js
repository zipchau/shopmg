import axios from 'axios'
import * as types from '../mutation-types'
import Swal from 'sweetalert2'

// state
export const state = {
  listProductCategory: [],
  listProductCategoryParent:[],
  productCategoryById: [],
  categoryParentToChild: [],
}

// getters
export const getters = {
  listProductCategory: state => state.listProductCategory,
  listProductCategoryParent: state => state.listProductCategoryParent.data,
  productCategoryById: state => state.productCategoryById.data,
  categoryParentToChild: state => state.categoryParentToChild.data
}

// mutations
export const mutations = {
  [types.FETCH_PRODUCTCATEGORY] (state, { data }) {
    state.listProductCategory = data
  },
  [types.UPDATE_PRODUCTCATEGORY] (state, { data }) {
    state.productCategoryById = data
  },
  [types.FETCH_PRODUCTCATEGORY_PARENT] (state, { data }) {
    state.listProductCategoryParent = data
  },
  [types.FETCH_CATEGORYPARENTTOCHILD] (state, { data }) {
    state.categoryParentToChild = data
  }
}

// actions
export const actions = {
  async fetchProductCategory ({ commit },page) {
      const { data } = await axios.get('/api/product-category?page='+page.page)
      commit(types.FETCH_PRODUCTCATEGORY, { data: data })
  },
  async fetchProductCategoryById ({ commit },id) {
      const { data } = await axios.get('/api/product-category/'+id.id)
      commit(types.UPDATE_PRODUCTCATEGORY, { data: data })
  },
  async fetchProductCategoryParent ({ commit },id){
    const { data } = await axios.get('/api/product-category/parent/'+id.id)
      commit(types.FETCH_PRODUCTCATEGORY_PARENT, { data: data })
  },
  async fetchProductParentToChild ({ commit }) {
      const { data } = await axios.get('/api/product-category/parenttochild')
      commit(types.FETCH_CATEGORYPARENTTOCHILD, { data: data })
  },
  async removeProductCategory ({ dispatch,commit },dataCompact) {
    const { data } =  await axios.delete('/api/product-category/'+dataCompact.id)
    if(data.status == true){
      Swal.fire(
        'Deleted!',
        'Your file has been deleted!',
        'success');
      dispatch('fetchProductCategory',{page:dataCompact.page})
    }else{
      Swal.fire(
        'Oops...',
        'Something went wrong!',
        'error'
      )
    }
  },
}
