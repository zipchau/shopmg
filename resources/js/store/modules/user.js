import axios from 'axios'
import * as types from '../mutation-types'

// state
export const state = {
  listUser: [],
  userById: []
}

// getters
export const getters = {
  listUser: state => state.listUser,
  userById: state => state.userById.data
}

// mutations
export const mutations = {
  [types.FETCH_USER] (state, { user }) {
    state.listUser = user
  },
  [types.UPDATE_USER] (state, { user }) {
    state.userById = user
  }
}

// actions
export const actions = {
  async fetchUser ({ commit },page) {
      const { data } = await axios.get('/api/user?page='+page.page)
      commit(types.FETCH_USER, { user: data })
  },

  async fetchUserById ({ commit },id) {
      const { data } = await axios.get('/api/user/'+id.id)
      commit(types.UPDATE_USER, { user: data })
  }
}
