// auth.js
export const LOGOUT = 'LOGOUT'
export const SAVE_TOKEN = 'SAVE_TOKEN'
export const FETCH_USER = 'FETCH_USER'
export const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS'
export const FETCH_USER_FAILURE = 'FETCH_USER_FAILURE'
export const UPDATE_USER = 'UPDATE_USER'
// lang.js
export const SET_LOCALE = 'SET_LOCALE'
//category_product
export const FETCH_PRODUCTCATEGORY = 'FETCH_PRODUCTCATEGORY'
export const UPDATE_PRODUCTCATEGORY = 'UPDATE_PRODUCTCATEGORY'
export const FETCH_PRODUCTCATEGORY_PARENT = 'FETCH_PRODUCTCATEGORY_PARENT'
export const FETCH_CATEGORYPARENTTOCHILD = 'FETCH_CATEGORYPARENTTOCHILD'
//product
export const FETCH_PRODUCT = 'FETCH_PRODUCT'
export const UPDATE_PRODUCT = 'UPDATE_PRODUCT'
//shop
export const FETCH_LISTFORPRODUCT = 'FETCH_LISTFORPRODUCT'
export const FETCH_CONFIG_SHOP = 'FETCH_CONFIG_SHOP'
export const FETCH_SHOP = 'FETCH_SHOP'
export const UPDATE_SHOP = 'UPDATE_SHOP'